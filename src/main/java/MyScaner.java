import java.io.*;
import java.util.Scanner;

public class MyScaner {
    public static void main(String[] args) throws IOException {

        PrintWriter outputStream = null;

        //Вчера моя черепаха Мария съела 12 гранул корма
        //Количество украинцев в возрасте 12 лет и старше, которые сообщили, что курят, в 2019 году составляло 5 559 200 человек

        Scanner scan = new Scanner(System.in);
        scan = new Scanner(scan.nextLine().replaceAll(" ", ""));

        try {
            outputStream = new PrintWriter(new FileWriter("out.txt",true));

            scan.useDelimiter(("\\D+"));

            while (scan.hasNextInt()) {
                outputStream.println(scan.nextInt());
            }
        } finally {
            if (outputStream != null) {
                outputStream.close();
            }
            scan.close();
        }
    }
}
